import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import reduxStore from './services/store';
import './App.css';
import Login from './pages/login';
import Home from './pages/home';
import SignUp from './pages/signup';

function App() {

  return (
    <Provider store={reduxStore}>
      <Router>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/login' component={Login} />
          <Route path='/signup' component={SignUp} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
