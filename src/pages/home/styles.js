import {makeStyles} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100vw', minHeight: `100vh`,
        display: 'flex', justifyContent: 'center', alignItems: 'center'
    }
}));

export default useStyles;