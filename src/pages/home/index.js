import { Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import useStyles from "./styles"

const Home = () => {

    const classes = useStyles();

    return(
        <div className={classes.root}>
            <Link to='/login'>
                <Typography variant='h3'>Login</Typography>
            </Link>
        </div>
    )
}

export default Home