import { Button, Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import Card from "../../components/Card";
import TextInput from "../../components/TextInput";
import useStyles from "./styles"

const Login = () => {

    const classes = useStyles();

    return(
        <div className={classes.root}>
            <Card>
                <Typography variant='h4'>Login</Typography>
                <div className={classes.flexCol}>
                    <TextInput label='E-Mail' />
                    <TextInput label='Password' />
                    <Button variant='contained' disableElevation color='primary' style={{marginTop: 16}}>Login</Button>
                </div>
                <Link to='/signup'>Create Account</Link>
            </Card>
        </div>
    )
}

export default Login