import {makeStyles} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100vw', minHeight: `100vh`,
        display: 'flex', justifyContent: 'center', alignItems: 'center'
    },
    flexCol: {
        display: 'flex',
        flexDirection: 'column',
        marginBlock: theme.spacing(2),
        gap: `8px`
    }
}));

export default useStyles;