import { Button, Typography } from "@material-ui/core";
import { useDispatch } from "react-redux";
import {signupUser} from '../../services/auth/actions';
import Card from "../../components/Card";
import TextInput from "../../components/TextInput";
import useStyles from "./styles"
import { useState } from "react";

const SignUp = () => {

    const classes = useStyles();
    const dispatch = useDispatch();
    const [inputData, setInputData] = useState({email: '', pwd: ''})

    return(
        <div className={classes.root}>
            <Card>
                <Typography variant='h4'>SignUp</Typography>
                <div className={classes.flexCol}>
                    <TextInput label='E-Mail' />
                    <TextInput label='Password' />
                    <Button variant='contained' disableElevation color='primary' style={{marginTop: 16}} onClick={()=>dispatch(signupUser(inputData))} >Sign Up</Button>
                </div>
            </Card>
        </div>
    )
}

export default SignUp;