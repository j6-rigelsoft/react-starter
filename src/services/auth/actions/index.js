import { ACTION_TYPES } from "../../../constants";

export const loginUser = (data) => ({
    type: ACTION_TYPES.LOGIN,
    payload: data
})

export const signupUser = (data) => ({
    type: ACTION_TYPES.SIGN_UP,
    payload: data
})