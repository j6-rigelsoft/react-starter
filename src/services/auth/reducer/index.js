import { ACTION_TYPES } from '../../../constants';

const initState = {
    users: localStorage.getItem('users'), activeUser: {}
}

const authReducer = (state=initState, action) => {
    switch(action.type){
        case ACTION_TYPES.LOGIN: {
            return {...initState}
        }
        case ACTION_TYPES.SIGN_UP: {
            let tempUser = {email: action?.payload?.email, pwd: action?.payload?.pwd};
            let tempUsers = [...state.users]; //Object.assign([], state.users); state.users.push()
            tempUsers.push(tempUser);
            localStorage.setItem('users', tempUsers)
            return {...state, users: tempUsers}
        }
        default: {
            return state;
        }
    }
}

export default authReducer;