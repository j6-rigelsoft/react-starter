import { TextField } from "@material-ui/core";
import useStyles from "./styles";

const TextInput = (props) => {

    const classes = useStyles()

    return(
        <TextField className={classes.root} fullWidth {...props}/>
    )
}

export default TextInput;