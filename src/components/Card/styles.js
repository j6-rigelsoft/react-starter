import {makeStyles} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        border: `1px solid ${theme.palette.secondary.main}`,
        minWidth: 320,
        padding: theme.spacing(2)
    }
}));

export default useStyles;