import { Card as MUICard } from "@material-ui/core";
import useStyles from "./styles";

const Card = (props) => {

    const classes = useStyles();

    return(
        <MUICard className={classes.root}>
            {props.children}
        </MUICard>
    )
}

export default Card;